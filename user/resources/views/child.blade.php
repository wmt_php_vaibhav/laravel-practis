@extends('header_footer')
@section('content')
    <h1>I am software developer.</h1>
@endsection

@foreach ($records as $user)
    <p>This is user {{ $user->name }}</p>
@endforeach

@forelse ($records as $user)
    <li>{{ $user->name }}</li>
@empty
    <p>No users</p>
@endforelse

@foreach ($records as $user)
    @if ($user->id == 1)
        @continue
    @endif

    <li>{{ $user->name }}</li>

    @if ($user->id == 5)
        @break
    @endif
@endforeach


@foreach ($records as $user)
    @continue($user->type == 1)

    <li>{{ $user->name }}</li>

    @break($user->number == 5)
@endforeach
