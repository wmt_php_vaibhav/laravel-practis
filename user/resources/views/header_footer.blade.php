<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <h1>start</h1>

    @component('abc')
        @slot('a')
            <h1>first</h1>
            @endslot

        @slot('b')
            <h1>second</h1>
            @endslot
        @endcomponent

    @yield('content')
    <h1>end</h1>
</div>
</body>
</html>
